		<!-- Bootstrap 4 -->
		<script src="<?php echo base_url(); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
		<!-- AdminLTE App -->
		<script src="<?php echo base_url(); ?>/dist/js/adminlte.min.js"></script>
		<script src="<?php echo base_url(); ?>/dist/js/jquery.validate.min.js"></script>

		<script type="text/javascript">
			$.validator.addMethod("pwcheck", function(value) {
			   return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
			       && /[a-z]/.test(value) // has a lowercase letter
			       && /\d/.test(value) // has a digit
			});

			$("#registerForm").validate({
				rules: {
					fName: "required",
					lName: "required",
					password: {
						required: true,
						pwcheck: true,
						minlength: 8
					},
					retype_password: {
						required: true,
						equalTo: "#password"
					},
					email: {
						required: true,
						email: true
					},
				},
				messages: {
					fName: "Please enter your firstname",
					lName: "Please enter your lastname",
					password: {
						required: "Please provide a password",
						minlength: "Your password must be at least 8 characters long",
					},
					retype_password: {
						required: "Please provide a password",
						minlength: "Your password must be at least 5 characters long",
						equalTo: "Please enter the same password as above."
					},
					email: "Please enter a valid email address",
				},
				errorPlacement: function (error, element) {
			      	if (element.next('input-group-append')) {
			          	error.insertAfter(element.next('.input-group-append'));
			      	}
			      	else {
			          	error.insertAfter(element);
			      	}
			  	}
			});
		</script>
	</body>
</html>