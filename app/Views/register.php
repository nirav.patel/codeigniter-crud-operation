<body class="hold-transition register-page">
  <div class="register-box">
    <div class="card card-outline card-primary">
      <div class="card-header text-center">
        <p class="h1"><b>Nirav</b>Practical</p>
      </div>
      <div class="card-body">
        <p class="login-box-msg">Register a new membership</p>
        <form id="registerForm" action="<?php echo base_url('login/auth') ?>" method="post">
          <div class="raw">
            <div class="input-group form-group col-md-6 col-sm-12">
              <input type="text" class="form-control" name="fName" placeholder="First name">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group form-group col-md-6 col-sm-12">
              <input type="text" class="form-control" name="lName" placeholder="Last name">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="raw">
            <div class="input-group form-group col-md-12 col-sm-12">
              <input type="email" class="form-control" name="email" placeholder="Email">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="raw">
            <div class="input-group form-group col-md-6 col-sm-12">
              <input type="password" class="form-control" name="password" placeholder="Password">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="input-group form-group col-md-6 col-sm-12">
              <input type="password" class="form-control" name="retype_password" placeholder="Retype password">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="raw">
            <div class="input-group form-group col-md-12 col-sm-12">
              <input type="text" class="form-control" name="phone" placeholder="Phone">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-phone"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="raw">
            <div class="input-group form-group col-md-12 col-sm-12">
              <input type="text" class="form-control" name="address" placeholder="Address">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-map-marker"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="raw">
            <div class="input-group form-group col-md-4 col-sm-12">
              <select class="form-control select2bs4" style="width: 100%;">
                <option value="">Select Country</option>
                <option value="Alabama">Alabama</option>
                <option value="Alaska">Alaska</option>
                <option value="California">California</option>
                <option value="Delaware">Delaware</option>
                <option value="Tennessee">Tennessee</option>
                <option value="Texas">Texas</option>
                <option value="Washington">Washington</option>
              </select>
            </div>
            <div class="input-group form-group col-md-4 col-sm-12">
              <select class="form-control select2bs4" style="width: 100%;">
                <option value="">Select State</option>
                <option value="Alabama">Alabama</option>
                <option value="Alaska">Alaska</option>
                <option value="California">California</option>
                <option value="Delaware">Delaware</option>
                <option value="Tennessee">Tennessee</option>
                <option value="Texas">Texas</option>
                <option value="Washington">Washington</option>
              </select>
            </div>
            <div class="input-group form-group col-md-4 col-sm-12">
              <select class="form-control select2bs4" style="width: 100%;">
                <option value="">Select City</option>
                <option value="Alabama">Alabama</option>
                <option value="Alaska">Alaska</option>
                <option value="California">California</option>
                <option value="Delaware">Delaware</option>
                <option value="Tennessee">Tennessee</option>
                <option value="Texas">Texas</option>
                <option value="Washington">Washington</option>
              </select>
            </div>
          </div>
          <div class="raw">
            <div class="input-group form-group col-md-6 col-sm-12">
              <input type="text" class="form-control" name="zipcode" placeholder="Zipcode">
            </div>
            <div class="input-group form-group col-md-6 col-sm-12">
              <label>Gender:&nbsp;</label>
              <div class="form-group">
                <div class="icheck-primary d-inline">
                  <input type="radio" id="radioPrimary1" name="gender" value="1">
                  <label for="radioPrimary1">Male</label>
                </div>
                <div class="icheck-primary d-inline">
                  <input type="radio" id="radioPrimary2" name="gender" value="2">
                  <label for="radioPrimary2">Female</label>
                </div>
                <div class="icheck-primary d-inline">
                  <input type="radio" id="radioPrimary3" name="gender" value="3">
                  <label for="radioPrimary3">Transgender</label>
                </div>
              </div>
            </div>
          </div>
          <div class="raw">
            <div class="input-group form-group col-md-12 col-sm-12">
              <input type="file" class="form-control" name="image">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-image"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <!-- <div class="col-8">
              <div class="icheck-primary">
                <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                <label for="agreeTerms">I agree to the <a href="#">terms</a></label>
              </div>
            </div> -->
            <!-- /.col -->
            <div class="col-4">
              <button type="submit" class="btn btn-primary btn-block">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
        <!-- <div class="social-auth-links text-center">
          <a href="#" class="btn btn-block btn-primary"><i class="fab fa-facebook mr-2"></i> Sign up using Facebook</a>
          <a href="#" class="btn btn-block btn-danger"><i class="fab fa-google-plus mr-2"></i>Sign up using Google+</a>
        </div> -->
        <p></p>
        <p>Already have an account? <a href="<?php echo base_url('login') ?>" class="text-center">Login</a></p>
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>
  <!-- /.register-box -->
  <!-- jQuery -->
  <script src="<?php echo base_url(); ?>/plugins/jquery/jquery.min.js"></script>
